/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.diccionario.Rest;

import com.mycompany.diccionario.dao.DiccionarioJpaController;
import com.mycompany.diccionario.entity.Diccionario;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

import javax.ws.rs.core.Response;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import oxford.Palabra;

/**
 *
 * @author paula
 */
@Path("palabra")
public class DiccionarioRest {

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response listaPalabras() {
        DiccionarioJpaController dao = new DiccionarioJpaController();
        List<Diccionario> lista = dao.findDiccionarioEntities();
        return Response.ok(200).entity(lista).build();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{palabra}")
    public Response buscarSignificado(@PathParam("palabra") String palabra) {

        Client client = ClientBuilder.newClient();
        WebTarget myResourse = client.target("https://od-api.oxforddictionaries.com:443/api/v2/entries/es/" + palabra);

        Palabra resultadoPalabra = myResourse.request(MediaType.APPLICATION_JSON)
                .header("app_id", "c3451c60")
                .header("app_key", "3e7414667b40722f0b6e39995591353a")
                .get(Palabra.class);

        String significado = resultadoPalabra.getResults().get(0)
                .getLexicalEntries().get(0)
                .getEntries().get(0)
                .getSenses().get(0)
                .getDefinitions().toString();

        significado = significado.replace("[", "").replace("]", "");
        
        return Response.ok(200).entity(significado).build();

    }
    
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response crear(Diccionario palabra) {

        DiccionarioJpaController dao = new DiccionarioJpaController();

        try {
            dao.create(palabra);
        } catch (Exception ex) {
            Logger.getLogger(DiccionarioRest.class.getName()).log(Level.SEVERE, null, ex);
        }

        return Response.ok(200).entity(palabra).build();

    }

}
