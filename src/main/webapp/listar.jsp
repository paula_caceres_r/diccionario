<%-- 
    Document   : listar
    Created on : 22-10-2021, 21:23:59
    Author     : paula
--%>

<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@page import="com.mycompany.diccionario.entity.Diccionario"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    List<Diccionario> lista = (List<Diccionario>) request.getAttribute("significados");

    Iterator<Diccionario> itlista = lista.iterator();

    java.text.DateFormat df = new java.text.SimpleDateFormat("dd/MM/yyyy");
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <form class="form-horizontal" action="DiccionarioController" method="POST">
            <h1 style="background-color:Orange;">Hitorial de busquedas</h1>
            <br>
            <table border="1">
                <thead>
                <th>Palabra</th>
                <th>Significado</th>
                <th>Fecha de consulta</th>
                </thead>
                <tbody>
                    <% while (itlista.hasNext()) {
                            Diccionario ins = itlista.next();%>
                    <tr>
                        <td><%= ins.getPalabra()%></td>
                        <td><%= ins.getSignificado()%></td>
                        <td><%= df.format(ins.getFecha())%></td>
                    </tr>
                    <%}%>
                </tbody>
            </table>
            <br>
            <button type="submit" name="accion" value="volver" class="btn btn-success">Buscar otra palabra</button>

        </form>
    </body>
</html>
