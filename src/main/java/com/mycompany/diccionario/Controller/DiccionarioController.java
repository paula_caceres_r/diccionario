/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.diccionario.Controller;

import com.mycompany.diccionario.Rest.DiccionarioRest;
import com.mycompany.diccionario.dao.DiccionarioJpaController;
import com.mycompany.diccionario.entity.Diccionario;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.Response;

/**
 *
 * @author paula
 */
@WebServlet(name = "DiccionarioController", urlPatterns = {"/DiccionarioController"})
public class DiccionarioController extends HttpServlet {

    private static final Logger logger = Logger.getLogger(DiccionarioController.class.getName());

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet DiccionarioController</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet DiccionarioController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

    private void obtenerHistorial(HttpServletRequest request, String accion, HttpServletResponse response) throws IOException, ServletException {
        DiccionarioRest dr = new DiccionarioRest();
        Response respuestaSignificado = dr.listaPalabras();

        List<Diccionario> significados = (List<Diccionario>) respuestaSignificado.getEntity();
        request.setAttribute("significados", significados);

        request.getRequestDispatcher("listar.jsp").forward(request, response);

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String accion = request.getParameter("accion");

        if (accion.equals("buscar")) {
            logger.info("buscando definicion");
            buscarDefinicion(request, response);
        }

        if (accion.equals("historial")) {
            logger.info("listar historial");
            obtenerHistorial(request, accion, response);
        }

        if (accion.equals("volver")) {
             logger.info("volviendo a pagina inicio");
            request.getRequestDispatcher("index.jsp").forward(request, response);
        }

    }

    private void buscarDefinicion(HttpServletRequest request, HttpServletResponse response) throws ServletException {
        try {
            String significado = "";
            DiccionarioJpaController dao = new DiccionarioJpaController();

            String palabra = request.getParameter("palabra");

            try {
                DiccionarioRest dr = new DiccionarioRest();
                Response respuestaSignificado = dr.buscarSignificado(palabra);
                significado = (String) respuestaSignificado.getEntity();
            } catch (Exception ex) {
                throw new ServletException("Error al llamar al servicio externo " + ex.getMessage());
            }

            Diccionario diccionario = new Diccionario();
            diccionario.setId(generarId());
            diccionario.setPalabra(palabra);
            diccionario.setSignificado(significado);
            diccionario.setFecha(new Date());
              

            try {
                //guardar en la bd
                dao.create(diccionario);
            } catch (Exception ex) {
                throw new ServletException("Error de BD " + ex.getMessage());
            }

            request.setAttribute("diccionario", diccionario);
            request.getRequestDispatcher("Resultado.jsp").forward(request, response);

        } catch (Exception ex) {
            logger.log(Level.SEVERE, null, ex);
            throw new ServletException(ex.getMessage());
        }
    }

    private String generarId() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMddHHmmssSSS");
        return LocalDateTime.now().format(formatter);
    }

}
