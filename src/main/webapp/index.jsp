<%-- 
    Document   : index
    Created on : 20-10-2021, 22:45:19
    Author     : paula
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Diccionario</h1>
        <form class="form-horizontal" action="DiccionarioController" method="POST" >
            <h1 style="background-color:Orange;">¿Que palabra desea buscar?</h1>
            <div class="form-group">
                <label class="control-label col-sm-2" for="palabra">Ingresar la palabra:</label >
                <br>
                <div class="col-sm-10">
                    <input type="text" name="palabra" required="true" class="form-control" id="palabra">
                </div>
            </div>
            <br>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" name="accion" value="buscar" class="btn btn-success">Buscar</button>
                </div>
            </div>
        </form>
        <br>
        <hr>

        <footer style="text-align: center;">
            <p>Paula Cáceres R.</p>           
            <a href="https://bitbucket.org/paula_caceres_r/diccionario/src/master/" target="_blank">Codigo Fuente</a>
        </footer>
    </body>

</html>
