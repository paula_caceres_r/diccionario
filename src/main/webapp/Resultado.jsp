<%-- 
    Document   : Resultado
    Created on : 21-10-2021, 12:02:39
    Author     : paula
--%>

<%@page import="oxford.Palabra"%>
<%@page import="com.mycompany.diccionario.entity.Diccionario"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    
     Diccionario diccionario = (Diccionario) request.getAttribute("diccionario");
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <form class="form-horizontal" action="DiccionarioController"  method="POST">
            <h1 style="background-color:Orange;">Definición</h1>
            <p>
                <span>Palabra consultada : <b><%= diccionario.getPalabra()%></b></span>
                <br>
                <span>Significado : <b><%= diccionario.getSignificado()%></b></span>
                <br>
                <small>[Fuente : <i>Oxford Dictionaries</i>]</small>
            </p>
            <br>
            <button type="submit" name="accion" value="volver" class="btn btn-success">Buscar otra palabra</button>
            <button type="submit" name="accion" value="historial" class="btn btn-info">Historial de busquedas</button>
        </form>
    </body>
</html>
