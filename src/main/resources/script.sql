-- Table: public.diccionario

-- DROP TABLE public.diccionario;

CREATE TABLE IF NOT EXISTS diccionario
(
	id VARCHAR NOT NULL,
    palabra VARCHAR NOT NULL,
    significado VARCHAR,
	fecha timestamp,
    CONSTRAINT diccionario_pkey PRIMARY KEY (id)
);
 